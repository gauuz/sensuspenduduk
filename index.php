<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Sensus Penduduk</title>

  <!-- Bootstrap -->
  <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of 
       HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Custom CSS -->
  <link href="assets/style.css" rel="stylesheet">

</head>
<body>
  
  <!-- container -->
  <div class="container">

      <!-- Bagian Header -->
      <div class="row">
          <div class="col-md-12 header" id="site-header">
              <!-- isi header -->
              <header>
                  <h1 class="title-site">Sensus Penduduk</h1>
                  <p class="description">Aplikasi untuk sensus penduduk Arkademy</p>
              </header>
              <nav class="menus">
                  <ul>
                      <li><a href="index.php">Home</a></li>
                      <?php

                      if(isset($_SESSION["logged_in"])){
                      	?>
                      	<li><a href="logout.php">Logout ( <?=$_SESSION["login_email"]; ?> ) </a></li>
                      	<?php
                      }else{
                      	//nothing
                      }
                      ?>

                      
                  </ul>
              </nav>
          </div>
      </div>
      <!-- End Bagian Header -->

      <!-- Bagian Wrapper 2 kolom -->
      <div class="row">
      	<?php

      	if(isset($_SESSION["logged_in"])){
          include "incl/sidebar.php";
      		if($_GET) {
            switch ($_GET['page']){
              case 'data_penduduk' :        
                if(!file_exists ("incl/penduduk.php")) die ("Halaman tidak ditemukan"); 
                include "incl/penduduk.php";            
                            
              break;

              case 'data_person' :        
                if(!file_exists ("incl/person.php")) die ("Halaman tidak ditemukan"); 
                include "incl/person.php";            
                            
              break;

              case 'add_person' :        
                if(!file_exists ("incl/add_person.php")) die ("Halaman tidak ditemukan"); 
                include "incl/add_person.php";            
                            
              break;

              case 'submit_person' :        
                if(!file_exists ("incl/submit_person.php")) die ("Halaman tidak ditemukan"); 
                include "incl/submit_person.php";            
                            
              break;

              case 'edit_person' :        
                if(!file_exists ("incl/edit_person.php")) die ("Halaman tidak ditemukan"); 
                include "incl/edit_person.php";            
                            
              break;

              case 'edit_submit_person' :        
                if(!file_exists ("incl/edit_submit_person.php")) die ("Halaman tidak ditemukan"); 
                include "incl/edit_submit_person.php";            
                            
              break;

              case 'delete_person' :        
                if(!file_exists ("incl/delete_person.php")) die ("Halaman tidak ditemukan");
                include "incl/delete_person.php";            
                            
              break;

              case 'data_region' :        
                if(!file_exists ("incl/region.php")) die ("Halaman tidak ditemukan"); 
                include "incl/region.php";            
                            
              break;

              case 'add_region' :        
                if(!file_exists ("incl/add_region.php")) die ("Halaman tidak ditemukan"); 
                include "incl/add_region.php";            
                            
              break;

              case 'submit_region' :        
                if(!file_exists ("incl/submit_region.php")) die ("Halaman tidak ditemukan"); 
                include "incl/submit_region.php";            
                            
              break;

              case 'edit_region' :        
                if(!file_exists ("incl/edit_region.php")) die ("Halaman tidak ditemukan"); 
                include "incl/edit_region.php";            
                            
              break;

              case 'edit_submit_region' :        
                if(!file_exists ("incl/edit_submit_region.php")) die ("Halaman tidak ditemukan"); 
                include "incl/edit_submit_region.php";            
                            
              break;

              case 'delete_region' :        
                if(!file_exists ("incl/delete_region.php")) die ("Halaman tidak ditemukan");
                include "incl/delete_region.php";            
                            
              break;

              case 'data_penduduk_filter' :        
                if(!file_exists ("incl/data_penduduk_filter.php")) die ("Halaman tidak ditemukan");
                include "incl/data_penduduk_filter.php";            
                            
              break;

              default:
                 
                if(!file_exists ("incl/home.php")) die ("Halaman tidak ditemukan"); 
                include "incl/home.php";
                          
              break;
            }
          }else{
            if(!file_exists ("incl/home.php")) die ("Halaman tidak ditemukan"); 
            include "incl/home.php";
          }
      	}else{
      		?>
          <div class="col-md-4 center-block">
            <form action="login.php" method="post">
             <div class="form-group">
               <label for="email">email:</label>
               <input type="text" class="form-control" name="email">
               <label for="password">password:</label>
               <input type="password" class="form-control" name="pass">
               <input type="submit" name="login" value="login" class="btn btn-info">
             </div>
            </form>
          </div>
      		<?php
      	}
      	?>
          
      </div>
      <!-- End Bagian wrapper -->
      
      <!-- Bagian footer -->
      <div class="row">
          <div class="col-md-12 footer" id="site-footer">
              <!-- isi footer -->
              <footer class="copyright text-center"><p>&copy; 2019 Agus Widiyanto</p></footer>
          </div>
      </div>
      <!-- End Bagian footer -->

  </div>
  <!-- end container -->
  
  <!-- jQuery lokal -->
  <script src="assets/jquery/jquery.min.js"></script>

  <script src="assets/bootstrap/js/bootstrap.min.js"></script>
  
  <!-- Custom JS -->
  <!-- Tidak ada custom js -->
</body>
</html>