<div class="col-md-8" id="site-content">
   <!-- isi content -->
   <article class="posts">
       <h2 class="title-post">Edit Person</h2>
       <div class="content">
        <?php
        $id_to_update = $_GET['id'];

        
          include "koneksi.php";

          $sql = "SELECT name, region_id, address, income FROM person WHERE id='$id_to_update'";
          $result=mysqli_query($con,$sql);
          $rowcount=mysqli_num_rows($result);
          if($rowcount > 0){
            //looping
            while($row = mysqli_fetch_array($result))
            {
              $nama_old = $row['name'];
              $region_old = $row['region_id'];
              $address_old = $row['address'];
              $income_old = $row['income'];
            }
            
          }else{
            //nothing
          }

          mysqli_close($con);
        


        ?>
          <form action="?page=edit_submit_person" method="post">
           <div class="form-group">
             <label for="name">Name:</label>
             <input type="text" class="form-control" name="name_person" value="<?php echo $nama_old; ?>">
           </div>
           <div class="form-group">
             <label for="region">Select Region (select one):</label>
                   <select class="form-control" name="sel_region">
                     <?php
                     
                       include "koneksi.php";

                       $sql = "SELECT id, name FROM regions";
                       $result=mysqli_query($con,$sql);
                       $rowcount=mysqli_num_rows($result);
                       if($rowcount > 0){
                         //looping
                         while($row = mysqli_fetch_array($result))
                         {
                           ?>
                           <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
                         <?php
                         }
                         
                       }else{
                         
                       }

                       mysqli_close($con);
                     

                     ?>
                   </select>
           </div>
           <div class="form-group">
             <label for="address">Address:</label>
             <input type="text" class="form-control" name="address" value="<?php echo $address_old; ?>">
           </div>
           <div class="form-group">
             <label for="income">Income:</label>
             <input type="number" class="form-control" name="income" value="<?php echo $income_old; ?>">
             <input type="hidden" name="id_to_update" value="<?php echo $id_to_update; ?>">
           </div>
           <button type="submit" class="btn btn-default">Update</button>
         </form> 
       </div>
   </article>
</div>