<div class="col-md-8" id="site-content">
   <!-- isi content -->
   <article class="posts">
       <h2 class="title-post">New Person</h2>
       <div class="content">
         <form action="?page=submit_person" method="post">
          <div class="form-group">
            <label for="name">Name:</label>
            <input type="text" class="form-control" name="name_person">
          </div>
          <div class="form-group">
            <label for="region">Select Region (select one):</label>
                  <select class="form-control" name="sel_region">
                    <?php
                    
                      include "koneksi.php";

                      $sql = "SELECT id, name FROM regions";
                      $result=mysqli_query($con,$sql);
                      $rowcount=mysqli_num_rows($result);
                      if($rowcount > 0){
                        //looping
                        while($row = mysqli_fetch_array($result))
                        {
                          ?>
                          <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
                        <?php
                        }
                        
                      }else{
                        
                      }

                      mysqli_close($con);
                    

                    ?>
                  </select>
          </div>
          <div class="form-group">
            <label for="address">Address:</label>
            <input type="text" class="form-control" name="address">
          </div>
          <div class="form-group">
            <label for="income">Income:</label>
            <input type="number" class="form-control" name="income">
          </div>
          <button type="submit" class="btn btn-default">Simpan</button>
        </form> 
       </div>
   </article>
</div>