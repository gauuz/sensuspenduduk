<div class="col-md-8" id="site-content">
   <!-- isi content -->
   <article class="posts">
       <h2 class="title-post">Data Region</h2>
       <h2><a href="?page=add_region" class="btn btn-success">Add</a></h2>

       <div class="content">
        <table class="table table-bordered">
            <thead>
              <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Option</th>
              </tr>
            </thead>
            <tbody>
              <?php
              
              if(isset($_SESSION["logged_in"])){
                include "koneksi.php";

                $sql = "SELECT id, name FROM regions";
                $result=mysqli_query($con,$sql);
                $rowcount=mysqli_num_rows($result);
                if($rowcount > 0){
                  //echo "Anda berhasil login!";
                  while($row = mysqli_fetch_array($result))
                  {
                    ?>

                  <tr>
                  <td><?php echo $row['id']; ?></td>
                  <td><?php echo $row['name']; ?></td>
                  <td><a href="?page=edit_region&id=<?php echo $row['id']; ?>" class="btn btn-info">edit</a>&nbsp;<a href="?page=delete_region&id=<?php echo $row['id']; ?>" class="btn btn-danger">delete</a></td>
                  </tr>

                  <?php
                  }
                  
                }else{
                  ?>
                  <h1>Belum ada data</h1>
                  <?php
                }

                mysqli_close($con);
              }else{
                //nothing
              }

              ?>
            </tbody>
          </table>
       </div>
   </article>
</div>