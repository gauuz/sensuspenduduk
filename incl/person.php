<div class="col-md-8" id="site-content">
   <!-- isi content -->
   <article class="posts">
       <h2 class="title-post">Data Person</h2>
       <h2><a href="?page=add_person" class="btn btn-success">Add</a></h2>

       <div class="content">
        <table class="table table-bordered">
            <thead>
              <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Income</th>
                <th>Region</th>
                <th>Address</th>
                <th>Option</th>
              </tr>
            </thead>
            <tbody>

              <?php
              
              if(isset($_SESSION["logged_in"])){
                include "koneksi.php";

                $sql = "SELECT `person`.id, `person`.`name` AS nama_penduduk, `regions`.name AS daerah, `person`.income AS income, `person`.address AS address FROM ((`person` JOIN `regions` ON `regions`.id = `person`.region_id))";
                $result=mysqli_query($con,$sql);
                $rowcount=mysqli_num_rows($result);
                if($rowcount > 0){
                  //echo "Anda berhasil login!";
                  while($row = mysqli_fetch_array($result))
                  {
                    ?>

                  <tr>
                  <td><?php echo $row['id']; ?></td>
                  <td><?php echo $row['nama_penduduk']; ?></td>
                  <td><?php echo $row['income']; ?></td>
                  <td><?php echo $row['daerah']; ?></td>
                  <td><?php echo $row['address']; ?></td>
                  <td><a href="?page=edit_person&id=<?php echo $row['id']; ?>" class="btn btn-info">edit</a>&nbsp;<a href="?page=delete_person&id=<?php echo $row['id']; ?>" class="btn btn-danger">delete</a></td>
                  </tr>

                  <?php
                  }
                  
                }else{
                  ?>
                  <h1>Belum ada data</h1>
                  <?php
                }

                mysqli_close($con);
              }else{
                //nothing
              }

              ?>

              
            </tbody>
          </table>
       </div>
   </article>
</div>