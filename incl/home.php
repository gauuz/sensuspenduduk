<div class="col-md-8" id="site-content">
   <!-- isi content -->
   <article class="posts">
       <h2 class="title-post">Data Daerah</h2>

       <div class="content">
       	Keterangan status:<br/>
       	Merah    : rata- rata pendapatan < 1.700.000<br/>
       	Kuning   : rata- rata pendapatan > 1.700.00 dan < 2.200.000<br/>
       	Hijau    : rata- rata pendapatan > 2.200.000<br/>

        <table class="table table-bordered">
            <thead>
              <tr>
                <th>id daerah</th>
                <th>Nama daerah</th>
                <th>Jumlah penduduk</th>
                <th>Total pendapatan</th>
                <th>Rata-rata pendapatan</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
            	<?php
            	
            	if(isset($_SESSION["logged_in"])){
            	  include "koneksi.php";

	              $sql = "SELECT `regions`.`id` AS region_id, `regions`.name AS region_name, COUNT(`person`.name) AS ttl_penduduk, SUM(`person`.income) AS ttl_pendapatan, REPLACE(FORMAT(AVG(`person`.income), 0), ',', '') AS rerata
	            	FROM ((`regions`
	            	JOIN `person` ON `person`.region_id = `regions`.id))
	            	GROUP BY `regions`.id";

            	  $result=mysqli_query($con,$sql);
            	  $rowcount=mysqli_num_rows($result);
            	  if($rowcount > 0){
            	    //echo "Anda berhasil login!";
            	    while($row = mysqli_fetch_array($result))
            	    {
            	      ?>

            	    <tr>
            	    <td><?php echo $row['region_id']; ?></td>
            	    <td><?php echo $row['region_name']; ?></td>
            	    <td><?php echo $row['ttl_penduduk']; ?></td>
            	    <td><?php echo $row['ttl_pendapatan']; ?></td>
            	    <td><?php echo $row['rerata']; ?></td>
            	    <td>
            	    <?php
            	      if($row['rerata'] < 1700000){
            	      	?> <mark style="background-color:red;">merah</mark>
            	      	<?php
            	      	}
            	      if($row['rerata'] > 1700000 && $row['rerata'] < 2200000){
            	      	?> <mark style="background-color:yellow;">kuning</mark>
            	      	<?php
            	      	}
            	      if($row['rerata'] > 2200000){
            	      	?> <mark style="background-color:green;">hijau</mark>
            	      	<?php
            	      	}
            	    ?>
            	    </td>
            	    </tr>

            	    <?php
            	    }
            	    
            	  }else{
            	    ?>
            	    <h1>Belum ada data</h1>
            	    <?php
            	  }

            	  mysqli_close($con);
            	}else{
            	  //nothing
            	}

            	?>

            </tbody>
          </table>
       </div>
   </article>
</div>