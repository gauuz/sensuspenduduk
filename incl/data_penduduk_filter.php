<div class="col-md-8" id="site-content">
   <!-- isi content -->
   <article class="posts">
       <h2 class="title-post">Data Penduduk Filter</h2>
       <div class="content">
        <table class="table table-bordered">
            <thead>
              <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Gaji</th>
                <th>Daerah</th>
              </tr>
            </thead>
            <tbody>

              <?php
              
              if(isset($_SESSION["logged_in"])){
                include "koneksi.php";
                $sel_region = $_POST['sel_region'];

                $sql = "SELECT `person`.id, `person`.`name` AS nama_penduduk, `person`.income AS gaji, `regions`.name AS daerah FROM ((`person` JOIN `regions` ON `regions`.id = `person`.region_id)) WHERE `regions`.id = '$sel_region'";
                $result=mysqli_query($con,$sql);
                $rowcount=mysqli_num_rows($result);
                if($rowcount > 0){
                  //echo "Anda berhasil login!";
                  while($row = mysqli_fetch_array($result))
                  {
                    ?>

                  <tr>
                  <td><?php echo $row['id']; ?></td>
                  <td><?php echo $row['nama_penduduk']; ?></td>
                  <td><?php echo $row['gaji']; ?></td>
                  <td><?php echo $row['daerah']; ?></td>
                  </tr>

                  <?php
                  }
                  
                }else{
                  ?>
                  <h1>Belum ada data</h1>
                  <?php
                }

                mysqli_close($con);
              }else{
                //nothing
              }

              ?>

              
            </tbody>
          </table>
       </div>
   </article>
</div>