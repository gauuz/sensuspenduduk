<div class="col-md-8" id="site-content">
   <!-- isi content -->
   <article class="posts">
       <h2 class="title-post">Data Penduduk Semua Daerah</h2>
       <form action="?page=data_penduduk_filter" method="post">
         Filter Daerah <select name="sel_region">
                     <?php
                     
                       include "koneksi.php";

                       $sql = "SELECT id, name FROM regions";
                       $result=mysqli_query($con,$sql);
                       $rowcount=mysqli_num_rows($result);
                       if($rowcount > 0){
                         //looping
                         while($row = mysqli_fetch_array($result))
                         {
                           ?>
                           <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
                         <?php
                         }
                         
                       }else{
                         
                       }

                       mysqli_close($con);
                     

                     ?>
                   
       </select> <input type="submit" name="lihat" value="lihat" class="btn btn-success">
       </form>

       <div class="content">
        <table class="table table-bordered">
            <thead>
              <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Gaji</th>
                <th>Daerah</th>
              </tr>
            </thead>
            <tbody>

              <?php
              
              if(isset($_SESSION["logged_in"])){
                include "koneksi.php";

                $sql = "SELECT `person`.id, `person`.`name` AS nama_penduduk, `person`.income AS gaji, `regions`.name AS daerah FROM ((`person` JOIN `regions` ON `regions`.id = `person`.region_id))";
                $result=mysqli_query($con,$sql);
                $rowcount=mysqli_num_rows($result);
                if($rowcount > 0){
                  //echo "Anda berhasil login!";
                  while($row = mysqli_fetch_array($result))
                  {
                    ?>

                  <tr>
                  <td><?php echo $row['id']; ?></td>
                  <td><?php echo $row['nama_penduduk']; ?></td>
                  <td><?php echo $row['gaji']; ?></td>
                  <td><?php echo $row['daerah']; ?></td>
                  </tr>

                  <?php
                  }
                  
                }else{
                  ?>
                  <h1>Belum ada data</h1>
                  <?php
                }

                mysqli_close($con);
              }else{
                //nothing
              }

              ?>

              
            </tbody>
          </table>
       </div>
   </article>
</div>