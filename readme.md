=================================
Credential
=================================
host: 'localhost'
user: 'root'
pass: ''
db_name: sensus_penduduk

=================================
Tools
=================================
text editor : sublime text 3
db editor : heidisql
php version : PHP Version 7.3.1
mysql version : 5.1.37
framework : native php

=================================
How to test ?
=================================
1. CREATE db by run sql script
2. Copy folder 'sensuspenduduk'
3. Sesuaikan koneksi.php
4. Akses via browser
5. Done

=================================
user and password
=================================
user : admin1@email.com
pass : admin1

user : admin2@email.com
pass : admin2

=================================

Thank you :D