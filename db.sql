-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.1.37 - Source distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for sensus_penduduk
CREATE DATABASE IF NOT EXISTS `sensus_penduduk` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `sensus_penduduk`;

-- Dumping structure for table sensus_penduduk.person
CREATE TABLE IF NOT EXISTS `person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `region_id` int(11) NOT NULL,
  `address` varchar(255) NOT NULL,
  `income` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumping data for table sensus_penduduk.person: 7 rows
/*!40000 ALTER TABLE `person` DISABLE KEYS */;
INSERT INTO `person` (`id`, `name`, `region_id`, `address`, `income`, `created_at`) VALUES
	(1, 'Agus Widiyanto', 1, 'Perum Kota Permata Blok D6/5x', 1800000, '2019-03-16 00:04:00'),
	(2, 'Nur Widiyanto', 3, 'RT 08/07 Cisalada, Kec. Mulyamekar', 4500000, '2019-03-16 09:50:43'),
	(3, 'Desi Permatasari', 3, 'Jln. Kebahagiaan no.99, Jakarta Barat', 5000000, '2019-03-16 09:53:54'),
	(5, 'Yana Suryana', 4, 'Jln. Sendirian no.77', 1900000, '2019-03-16 10:07:20'),
	(6, 'Faira Kaisa Mukti', 1, 'Jln. Merdeka no. 56', 3250009, '2019-03-16 10:09:20'),
	(7, 'Madina Kayla Putri', 6, 'Jl. Zambrud no.7', 6500000, '2019-03-16 10:06:45'),
	(8, 'Gamaliel', 3, 'Jl. Jakarta', 100000, '2019-03-16 16:21:24');
/*!40000 ALTER TABLE `person` ENABLE KEYS */;

-- Dumping structure for table sensus_penduduk.regions
CREATE TABLE IF NOT EXISTS `regions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table sensus_penduduk.regions: 6 rows
/*!40000 ALTER TABLE `regions` DISABLE KEYS */;
INSERT INTO `regions` (`id`, `name`, `created_at`) VALUES
	(1, 'Purwakarta', '2019-03-16 09:59:22'),
	(2, 'Bandung', '2019-03-16 09:59:38'),
	(3, 'Jakarta', '2019-03-16 09:59:44'),
	(4, 'Tangerang', '2019-03-16 09:59:50'),
	(5, 'Purbalingga', '2019-03-16 09:59:56'),
	(6, 'Subang', '2019-03-16 10:00:03'),
	(7, 'Surabaya', '2019-03-16 15:54:44'),
	(8, 'Denpasar', '2019-03-16 15:57:44'),
	(9, 'Cilegon', '2019-03-16 15:58:44'),
	(10, 'Serang', '2019-03-16 16:01:44'),
	(11, 'Pekanbaru', '2019-03-16 16:07:43'),
	(12, 'Semarang', '2019-03-16 16:14:43');
/*!40000 ALTER TABLE `regions` ENABLE KEYS */;

-- Dumping structure for table sensus_penduduk.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table sensus_penduduk.users: 2 rows
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `email`, `password`, `created_at`) VALUES
	(1, 'admin1@email.com', 'admin1', '2019-03-16 09:57:39'),
	(2, 'admin2@email.com', 'admin2', '2019-03-16 09:58:18');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
